import { Routes } from '../../shares/types';
import { PopoverButtonBasic } from './basic';

export const POPOVER_BUTTON_ROUTES: Routes = {
	'popover-button': {
		basic: PopoverButtonBasic,
	},
};
