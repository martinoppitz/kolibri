import { KolAbbr } from './abbr/shadow';
import { KolAccordion } from './accordion/shadow';
import { KolAlert } from './alert/shadow';
import { KolAlertWc } from './alert/component';
import { KolAvatar } from './avatar/shadow';
import { KolAvatarWc } from './avatar/component';
import { KolBadge } from './badge/shadow';
import { KolBreadcrumb } from './breadcrumb/shadow';
import { KolButton } from './button/shadow';
import { KolButtonLink } from './button-link/shadow';
import { KolButtonWc } from './button/component';
import { KolCard } from './card/shadow';
import { KolCombobox } from './combobox/shadow';
import { KolDetails } from './details/shadow';
import { KolDrawer } from './drawer/shadow';
import { KolForm } from './form/shadow';
import { KolHeading } from './heading/shadow';
import { KolIcon } from './icon/shadow';
import { KolImage } from './image/shadow';
import { KolInputCheckbox } from './input-checkbox/shadow';
import { KolInputColor } from './input-color/shadow';
import { KolInputDate } from './input-date/shadow';
import { KolInputEmail } from './input-email/shadow';
import { KolInputFile } from './input-file/shadow';
import { KolInputNumber } from './input-number/shadow';
import { KolInputPassword } from './input-password/shadow';
import { KolInputRadio } from './input-radio/shadow';
import { KolInputRange } from './input-range/shadow';
import { KolInputText } from './input-text/shadow';
import { KolInputWc } from './input/component';
import { KolKolibri } from './kolibri/shadow';
import { KolLink } from './link/shadow';
import { KolLinkButton } from './link-button/shadow';
import { KolLinkWc } from './link/component';
import { KolModal } from './modal/shadow';
import { KolNav } from './nav/shadow';
import { KolPagination } from './pagination/shadow';
import { KolPopover } from './popover/component';
import { KolProgress } from './progress/shadow';
import { KolPopoverButton } from './popover-button/shadow';
import { KolQuote } from './quote/shadow';
import { KolSelect } from './select/shadow';
import { KolSingleSelect } from './single-select/shadow';
import { KolSkipNav } from './skip-nav/shadow';
import { KolSpin } from './spin/shadow';
import { KolSplitButton } from './split-button/shadow';
import { KolTabs } from './tabs/shadow';
import { KolTextarea } from './textarea/shadow';
import { KolToastContainer } from './toaster/shadow';
import { KolToolbar } from './toolbar/shadow';
import { KolTooltipWc } from './tooltip/component';
import { KolTree } from './tree/shadow';
import { KolTreeItem } from './tree-item/shadow';
import { KolTreeItemWc } from './tree-item/component';
import { KolTreeWc } from './tree/component';
import { KolVersion } from './version/shadow';

export const COMPONENTS = [
	KolAbbr,
	KolAccordion,
	KolAlert,
	KolAlertWc,
	KolAvatar,
	KolAvatarWc,
	KolBadge,
	KolBreadcrumb,
	KolButton,
	KolButtonLink,
	KolButtonWc,
	KolCard,
	KolCombobox,
	KolDetails,
	KolDrawer,
	KolForm,
	KolHeading,
	KolIcon,
	KolImage,
	KolInputCheckbox,
	KolInputColor,
	KolInputDate,
	KolInputEmail,
	KolInputFile,
	KolInputNumber,
	KolInputPassword,
	KolInputRadio,
	KolInputRange,
	KolInputText,
	KolInputWc,
	KolKolibri,
	KolLink,
	KolLinkButton,
	KolLinkWc,
	KolModal,
	KolNav,
	KolPagination,
	KolPopover,
	KolProgress,
	KolPopoverButton,
	KolQuote,
	KolSelect,
	KolSingleSelect,
	KolSkipNav,
	KolSpin,
	KolSplitButton,
	KolTabs,
	KolTextarea,
	KolToastContainer,
	KolToolbar,
	KolTooltipWc,
	KolTree,
	KolTreeItem,
	KolTreeItemWc,
	KolTreeWc,
	KolVersion,
];
