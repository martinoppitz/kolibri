export interface EventDetail {
	name: string;
	value: string;
}
