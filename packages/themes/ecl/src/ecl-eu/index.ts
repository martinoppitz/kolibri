import { KoliBri } from '@public-ui/components';
import globalCss from './global.scss';
import accordionCss from './components/accordion.scss';
import alertCss from './components/alert.scss';
import avatarCss from './components/avatar.scss';
import badgeCss from './components/badge.scss';
import breadcrumbCss from './components/breadcrumb.scss';
import buttonCss from './components/button.scss';
import buttonLinkCss from './components/button-link.scss';
import cardCss from './components/card.scss';
import ComboboxCss from './components/combobox.scss';
import detailsCss from './components/details.scss';
import drawerCss from './components/drawer.scss';
import formCss from './components/form.scss';
import headingCss from './components/heading.scss';
import inputCheckboxCss from './components/input-checkbox.scss';
import inputColorCss from './components/input-color.scss';
import inputDateCss from './components/input-date.scss';
import inputEmailCss from './components/input-email.scss';
import inputFileCss from './components/input-file.scss';
import inputNumberCss from './components/input-number.scss';
import inputPasswordCss from './components/input-password.scss';
import inputRadioCss from './components/input-radio.scss';
import inputRangeCss from './components/input-range.scss';
import inputTextCss from './components/input-text.scss';
import kolibriCss from './components/kolibri.scss';
import linkButtonCss from './components/link-button.scss';
import linkCss from './components/link.scss';
import modalCss from './components/modal.scss';
import navCss from './components/nav.scss';
import paginationCss from './components/pagination.scss';
import popoverButtonCss from './components/popover-button.scss';
import progressCss from './components/progress.scss';
import selectCss from './components/select.scss';
import singleSelectCss from './components/single-select.scss';
import skipNavCss from './components/skip-nav.scss';
import spinCss from './components/spin.scss';
import splitButtonCss from './components/split-button.scss';
import tableStatefulCss from './components/table-stateful.scss';
import tableStatelessCss from './components/table-stateless.scss';
import tabsCss from './components/tabs.scss';
import textareaCss from './components/textarea.scss';
import toastContainerCss from './components/toast-container.scss';
import toolbarCss from './components/toolbar.scss';
import treeCss from './components/tree.scss';
import treeItemCss from './components/tree-item.scss';

// Europa Component Library - European Union | https://ec.europa.eu/component-library/eu/
export const ECL_EU = KoliBri.createTheme('ecl-eu', {
	GLOBAL: globalCss,
	'KOL-ACCORDION': accordionCss,
	'KOL-ALERT': alertCss,
	'KOL-AVATAR': avatarCss,
	'KOL-BADGE': badgeCss,
	'KOL-BREADCRUMB': breadcrumbCss,
	'KOL-BUTTON': buttonCss,
	'KOL-BUTTON-LINK': buttonLinkCss,
	'KOL-CARD': cardCss,
	'KOL-COMBOBOX': ComboboxCss,
	'KOL-DETAILS': detailsCss,
	'KOL-DRAWER': drawerCss,
	'KOL-FORM': formCss,
	'KOL-HEADING': headingCss,
	'KOL-INPUT-CHECKBOX': inputCheckboxCss,
	'KOL-INPUT-COLOR': inputColorCss,
	'KOL-INPUT-DATE': inputDateCss,
	'KOL-INPUT-EMAIL': inputEmailCss,
	'KOL-INPUT-FILE': inputFileCss,
	'KOL-INPUT-NUMBER': inputNumberCss,
	'KOL-INPUT-PASSWORD': inputPasswordCss,
	'KOL-INPUT-RADIO': inputRadioCss,
	'KOL-INPUT-RANGE': inputRangeCss,
	'KOL-INPUT-TEXT': inputTextCss,
	'KOL-KOLIBRI': kolibriCss,
	'KOL-LINK': linkCss,
	'KOL-LINK-BUTTON': linkButtonCss,
	'KOL-MODAL': modalCss,
	'KOL-NAV': navCss,
	'KOL-PAGINATION': paginationCss,
	'KOL-POPOVER-BUTTON': popoverButtonCss,
	'KOL-PROGRESS': progressCss,
	'KOL-SELECT': selectCss,
	'KOL-SINGLE-SELECT': singleSelectCss,
	'KOL-SKIP-NAV': skipNavCss,
	'KOL-SPIN': spinCss,
	'KOL-SPLIT-BUTTON': splitButtonCss,
	'KOL-TABLE-STATEFUL': tableStatefulCss,
	'KOL-TABLE-STATELESS': tableStatelessCss,
	'KOL-TABS': tabsCss,
	'KOL-TEXTAREA': textareaCss,
	'KOL-TOAST-CONTAINER': toastContainerCss,
	'KOL-TOOLBAR': toolbarCss,
	'KOL-TREE': treeCss,
	'KOL-TREE-ITEM': treeItemCss,
});
